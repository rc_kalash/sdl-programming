#include <SDL2/SDL.h>
#include <SDL2/SDL_error.h>
#include <SDL2/SDL_filesystem.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_render.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include "Window.hpp"
#include "AssetServer.hpp"

int main(int argc, char** argv) {

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    printf("Error initializing SDL: %s",SDL_GetError());
  }

  /* 
  int imgInit = IMG_Init(IMG_INIT_PNG);

  if (imgInit == 0) {
    printf("Error init SDL_IMAGE: %s",IMG_GetError());
  }
  */

  // WINDOW

  Window window = Window{"Window Title",0,0,1920,1080,SDL_WINDOW_RESIZABLE};

  //SDL_Window* window{SDL_CreateWindow(
  //  "Hello Window", 0, 0, 1920, 1080, SDL_WINDOW_RESIZABLE)};

  // RENDERER
  SDL_Renderer* renderer = SDL_CreateRenderer(window.getWindow(), -1,SDL_RENDERER_PRESENTVSYNC);

  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 0);

  SDL_RenderSetLogicalSize(renderer,800,600);

  SDL_Rect textureRect;
  textureRect.x = 0;
  textureRect.y = 0;
  textureRect.w = 1920;
  textureRect.h = 1080;
  
  SDL_Event event;
  
  bool run{true};

  // TILE MAP -code-
  // ASSET LOADING


  std::string buffer{SDL_GetBasePath()};
  buffer.append("/resources/tileset-atlas-test.png");
  std::string name{"TILE"};


  SDL_Texture* tileTexture = AssetServer::instance().loadTexture(buffer,name,renderer);

  const int SIZE_X = 50;
  const int SIZE_Y = 38;
  const int TILE_SIZE = 16;

  int tilemap[SIZE_X][SIZE_Y];

  for (int x = 0; x < SIZE_X; x++) {
    for (int y = 0; y < SIZE_Y; y++) {
      tilemap[x][y] = rand() % 4 + 1; // Randomize betewen 1-4
    }
  }

  SDL_Rect tileRect[SIZE_X][SIZE_Y];

  for (int x = 0; x < SIZE_X; x++) {
    for (int y = 0; y < SIZE_Y; y++) {
      tileRect[x][y].x = x * TILE_SIZE;
      tileRect[x][y].y = y * TILE_SIZE;
      tileRect[x][y].h = TILE_SIZE;
      tileRect[x][y].w = TILE_SIZE;
     }
  }

  // Crear los rects del textureAtlas del tilemap
  SDL_Rect tile1;
  tile1.x = 0;
  tile1.y = 0;
  tile1.h = 16;
  tile1.w = 16;
  SDL_Rect tile2;
  tile2.x = 16;
  tile2.y = 0;
  tile2.h = 16;
  tile2.w = 32;
  SDL_Rect tile3;
  tile3.x = 0;
  tile3.y = 16;
  tile3.h = 32;
  tile3.w = 16;
  SDL_Rect tile4;
  tile4.x = 16;
  tile4.y = 16;
  tile4.h = 32;
  tile4.w = 32;
  // TILE MAP END



  printf("START GAME LOOP\n");

  while (run) {
    SDL_RenderClear(renderer);
    for (int x = 0; x < SIZE_X; x++) {
      for (int y = 0; y < SIZE_Y; y++) {
	switch (tilemap[x][y]) {
	case 1:
	  SDL_RenderCopy(renderer,tileTexture,&tile1,&tileRect[x][y]);
	  break;
	case 2:
	  SDL_RenderCopy(renderer,tileTexture,&tile2,&tileRect[x][y]);
	  break;
	case 3:
	  SDL_RenderCopy(renderer,tileTexture,&tile3,&tileRect[x][y]);
	  break;
	case 4:
	  SDL_RenderCopy(renderer,tileTexture,&tile4,&tileRect[x][y]);
	  break;
	}
      } 
    }
    SDL_RenderPresent(renderer);
    while(SDL_PollEvent(&event)){
      if(event.type == SDL_QUIT){
	run = false;
      }
    } 
  }
  SDL_DestroyRenderer(renderer);
  SDL_Quit();
  return 0;
}
