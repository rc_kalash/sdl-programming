#include "Window.hpp"
#include <SDL2/SDL.h>

SDL_Window* Window::getWindow() {
  return this -> window; 
}

Window::Window(const char* title,int posX, int posY, int width, int height,Uint32 flags){
  window = SDL_CreateWindow(title,posX,posY,width,height,flags);
}

Window::~Window(){
  SDL_DestroyWindow(this -> window);
}
