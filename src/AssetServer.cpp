#include "AssetServer.hpp"
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_error.h>
#include <SDL2/SDL_filesystem.h>
#include <iostream>
#include <memory>
#include <SDL2/SDL.h>
#include <string>
#include <map>

AssetServer& AssetServer::instance(){
  // TODO use smart pointer?
  //static std::unique_ptr<AssetServer> instance (new AssetServer{});
  static AssetServer* instance = new AssetServer{};

  return *instance;
}


SDL_Texture* AssetServer::getTexture(const std::string& textureName){
  return textures[textureName];
}

SDL_Texture* AssetServer::loadTexture(const std::string& path,const std::string& name, SDL_Renderer* renderer){

  if(textures.find(name) != textures.end()){
    return textures[name];
  }

  SDL_Texture* texture = IMG_LoadTexture(renderer,path.c_str());

  if (texture == nullptr) {
    std::cout << "Error loading Texture" << std::endl;
    SDL_LogError(0, "Error loading texture with name "); // FIX LOG LEVEL
    std::cout << IMG_GetError() << std::endl;
    return nullptr;
  }

  return textures.insert(std::make_pair(name,texture)).first -> second;
    
}

AssetServer::AssetServer(){
  int imgInit = IMG_Init(IMG_INIT_PNG);

  if (imgInit == 0) {
    printf("Error init SDL_IMAGE: %s",IMG_GetError());
  }

}


AssetServer::AssetServer(AssetServer const&){}


AssetServer::~AssetServer(){
  for(auto texture : textures){
    if(texture.second){
      SDL_DestroyTexture(texture.second);
      texture.second = nullptr;
    }
  }
}
